/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.content.impl;

import java.io.File;
import java.net.URI;
import java.util.Properties;

public final class SecureFileContentManager extends FileContentManager {


	protected URI contentFolderUri;


	public SecureFileContentManager() {
		super();
	}


	@Override
	public void initialize(final Properties properties_p) {
		super.initialize(properties_p);
		this.contentFolderUri = this.contentFolder.toURI().normalize();
	}


	@Override
	protected File resolveURI(final URI uri) {
		final File file;
		if (uri.isOpaque()) {
			// In the following case, we have to parse the URI by hand
			// When opaque, the URI is always relative remove the scheme from uri and build the correct relative path
			final String path = uri.toASCIIString().replaceFirst(uri.getScheme() + ":", "");
			// when leading slash does not appear, we should search in the defined path
			this.logger.debug("Relative Path used for reading content: " + path);
			file = new File(this.contentFolder, path);
		} else {
			final URI resultingURI_l = this.contentFolderUri.relativize(uri);
			this.logger.debug("URI relativized: " + resultingURI_l);
			if ((resultingURI_l.compareTo(uri) == 0) || (resultingURI_l.isAbsolute())) {
				this.logger.warn("Absolute uri " + uri + " does not containt path to Content Manager repository. Try to change its root to be inside content manager");
				final String chrootedPath_l = uri.getPath().substring(1, uri.getPath().length());
				file = new File(this.contentFolder, chrootedPath_l);
			} else {
				file = new File(this.contentFolder, resultingURI_l.getPath());
			}
		}
		return file;
	}


	@Override
	protected URI resolveFile(final File target) {
		final String uri = this.contentFolderUri.relativize(super.resolveFile(target)).toString();
		if (uri.startsWith(FileContentManager.FILE_SCHEME + ':')) {
			// Absolute URI
			return URI.create(uri);
		}
		return URI.create(FileContentManager.FILE_SCHEME + ':' + uri);
	}


}