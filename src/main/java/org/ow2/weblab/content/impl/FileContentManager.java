/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManagerAdapter;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * Default file implementation for ContentManagerAdapter.
 */
public class FileContentManager extends ContentManagerAdapter {


	public static final String LOCAL_CONTENT = "FileContentManager.LocalContent";


	public static final String NETWORK_SCHEMES = "http:ftp:https";


	public static final String FILE_SCHEME = "file";


	public static final String FOLDER_CONTENT_PATH_PROPERTY_NAME = "file.path";


	public static final String FOLDER_CONTENT_DEFAULT_PATH = "data/content";


	public static final String PREFIX = "weblab.";


	public static final String SUFFIX = ".content";


	protected File contentFolder = null;


	protected final Log logger;


	public FileContentManager() {
		this.logger = LogFactory.getLog(this.getClass());
	}


	@Override
	public void initialize(final Properties properties_p) {
		super.initialize(properties_p);
		String contentPath = System.getProperty(FileContentManager.FOLDER_CONTENT_PATH_PROPERTY_NAME);

		if (contentPath == null) {
			// check if in properties
			contentPath = properties_p.getProperty(FileContentManager.FOLDER_CONTENT_PATH_PROPERTY_NAME);
			if (contentPath == null) {
				// use default
				contentPath = FileContentManager.FOLDER_CONTENT_DEFAULT_PATH;
			}
		}
		this.checkAndBuildContentPath(contentPath);
	}



	@Override
	public URI create(final InputStream input, final Resource resource, final Map<String, Object> parameters) throws IOException {
		File target = null;
		if ((resource != null) && (parameters != null) && parameters.containsKey(FileContentManager.LOCAL_CONTENT)
				&& Boolean.valueOf(parameters.get(FileContentManager.LOCAL_CONTENT).toString()).booleanValue()) {
			final Value<String> source = new DublinCoreAnnotator(resource).readSource();
			if (source.hasValue()) {
				target = new File(source.firstTypedValue());
				if (!target.exists()) {
					this.logger.warn("Ask for local file storing but source does not exist " + target.getAbsolutePath() + ".");
					target = null;
				}
			} else {
				this.logger.warn("Ask for local file storing but no source found on resource " + resource.getUri() + ".");
			}
		}

		if (target == null) {
			target = this.createEmptyFile();
			FileUtils.copyInputStreamToFile(input, target);
		}

		return this.resolveFile(target);
	}


	@Override
	public InputStream read(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		// First try to save time by reading local file (does not throw error in case of bad URI)
		final File local = this.readLocalExistingFile(uri, resource, parameters);
		if (local != null) {
			return FileUtils.openInputStream(local);
		}

		// If not available (non local, non existing, non handled...) Retry but this time download file if needed and fail if needed too

		// URI does not have scheme specified
		if (!uri.isAbsolute()) {
			throw new IOException("Invalid URI, scheme is null in " + uri);
		}

		final String scheme = uri.getScheme();
		if (FileContentManager.NETWORK_SCHEMES.contains(scheme)) {
			return uri.toURL().openStream();
		} else if (!FileContentManager.FILE_SCHEME.equalsIgnoreCase(scheme)) {
			throw new IOException("Invalid URI scheme [" + scheme + "], only [" + FileContentManager.FILE_SCHEME + "] is valid.");
		} else {
			final File toRead = this.resolveURI(uri);
			if (!toRead.exists()) {
				throw new IOException("File [" + toRead.getPath() + "] does not exists.");
			}
			// Should be tested after existence. In the opposite, we miss the existence problem as isFile => exists AND normal file
			if (!toRead.isFile()) {
				throw new IOException("File [" + toRead.getPath() + "] is not a file.");
			}
			if (!toRead.canRead()) {
				throw new IOException("File [" + toRead.getPath() + "] is not readable.");
			}

			this.logger.debug("Strange that the uri " + uri + " has not been resolved at first try. Is-it a distributed folder?");
			return FileUtils.openInputStream(toRead);
		}

	}


	@Override
	public File readLocalExistingFile(final URI uri, final Resource resource, final Map<String, Object> parameters) {
		if (uri == null) {
			return null;
		}
		if (!uri.isAbsolute()) {
			return null;
		}
		if (!FileContentManager.FILE_SCHEME.equalsIgnoreCase(uri.getScheme())) {
			return null;
		}
		final File file = this.resolveURI(uri);
		if (file.exists() && file.canRead() && file.isFile()) {
			return file;
		}

		this.logger.debug("Content URI " + uri + " is resolved to a non existing local file.");
		return null;
	}


	@Override
	public URI update(final InputStream input, final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		if (!uri.isAbsolute()) {
			throw new IOException("Invalid URI, scheme is null in " + uri);
		}
		if (!FileContentManager.FILE_SCHEME.startsWith(uri.getScheme())) {
			throw new IOException("Unable to write a content with such a scheme " + uri);
		}
		final File file = this.resolveURI(uri);
		FileUtils.copyInputStreamToFile(input, file);

		return uri;
	}


	@Override
	public boolean delete(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		final File f = this.readLocalExistingFile(uri, resource, parameters);
		if (f == null) {
			this.logger.debug("Unable to delete content " + uri + ". Either it does not exist or protocol is not handled.");
			return false;
		}

		if (f.delete()) {
			return true;
		}

		this.logger.debug("Unable to delete existing content " + uri + ". Flag with delete on exit.");
		f.deleteOnExit();
		return false;
	}


	/**
	 * @param uri
	 *            The file content URI
	 * @return The file resolved absolutely or relatively based on the file URI provided.
	 */
	protected File resolveURI(final URI uri) {
		final File file;
		if (uri.isOpaque()) {
			// In the following case, we have to parse the URI by hand
			// When opaque, the URI is always relative remove the scheme from uri and build the correct relative path
			final String path = uri.toASCIIString().replaceFirst(uri.getScheme() + ":", "");
			// when leading slash does not appear, we should search in the defined path
			this.logger.debug("Relative Path used for reading content: " + path);
			file = new File(this.contentFolder, path);
		} else {
			// URI is fully parseable and thus begins with a leading /
			file = new File(uri);
		}
		return file;
	}



	/**
	 * @param target
	 *            The target file to be converted as URI
	 * @return The URI to be written in content manager
	 */
	protected URI resolveFile(final File target) {
		return target.toURI().normalize();
	}




	/**
	 * @return
	 * @throws IOException
	 */
	protected File createEmptyFile() throws IOException {
		final File folder = new File(this.contentFolder, String.valueOf(Math.round(9 * Math.random())));
		FileUtils.forceMkdir(folder);
		return File.createTempFile(FileContentManager.PREFIX, FileContentManager.SUFFIX, folder);
	}



	protected void checkAndBuildContentPath(final String contentFolderPath) throws WebLabUncheckedException {
		final File folder = new File(contentFolderPath);
		if (!folder.isAbsolute()) {
			this.logger.warn(contentFolderPath + " is a relative path, thus depends on working directory.");
		}

		try {
			FileUtils.forceMkdir(folder);
		} catch (final IOException ioe) {
			final String message = "Unable to create Content folder [" + folder + "].";
			this.logger.error(message, ioe);
			throw new WebLabUncheckedException(message, ioe);
		}
		if (!folder.exists()) {
			final String message = "Content folder [" + folder + "] does not exists or is not readable.";
			this.logger.error(message);
			throw new WebLabUncheckedException(message);
		}
		if (folder.isFile()) {
			final String message = "Content folder [" + folder + "] is a File instead of a directory.";
			this.logger.error(message);
			throw new WebLabUncheckedException(message);
		}
		if (!folder.canWrite()) {
			this.logger.warn("Cannot write in content folder [" + folder + "]. If the component only read it should be fine. Otherwise you will have some troubles later...");
		}

		this.contentFolder = folder;

		this.logger.info("Content path used " + this.contentFolder.getAbsolutePath());
	}

}