/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;

public final class ContentManager extends ContentManagerAdapter {


	// ////////////////////////////////
	// static part
	// ////////////////////////////////

	public static final String CONTENT_MANAGER_PROPERTIES = "contentManager.properties";


	public static final String DEFAULT_CONTENT_MANAGER = "org.ow2.weblab.content.impl.SecureFileContentManager";


	public static final String CONTENT_MANAGER_SYSTEM_PROPERTY = "weblab.content.manager";


	protected static final Log LOGGER = LogFactory.getLog(ContentManager.class);


	protected static ContentManager instance;


	/**
	 * Default starting method to get the ContentManager instance.
	 *
	 * @return the ContentManager instance
	 */
	public static synchronized ContentManager getInstance() {
		try {
			if (ContentManager.instance == null) {
				ContentManager.instance = new ContentManager();
			}
			return ContentManager.instance;
		} catch (final ClassNotFoundException cnfe) {
			throw new WebLabUncheckedException("Cannot found one of the implementation.", cnfe);
		} catch (final InstantiationException ie) {
			throw new WebLabUncheckedException("Cannot instanciate one of the implementation.", ie);
		} catch (final IllegalAccessException iae) {
			throw new WebLabUncheckedException("Error while constructing one of the implementation.", iae);
		}
	}


	// ////////////////////////////////
	// instance part
	// ////////////////////////////////

	protected ContentManagerInterface manager;


	/**
	 * The default constructor that initialises reader/writer.
	 *
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private ContentManager() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		String contentProperty = System.getProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY);

		if (contentProperty == null) {
			// search for contentManager.properties in local classpath
			final URL url = ContentManager.class.getClassLoader().getResource(ContentManager.CONTENT_MANAGER_PROPERTIES);
			if (url != null) {
				contentProperty = url.getFile();
			}
		}

		String contentManagerClass = ContentManager.DEFAULT_CONTENT_MANAGER;
		final Properties properties_l = new Properties();

		if (contentProperty != null) {
			// check if properties file
			final File propertiesFile = new File(contentProperty);
			if (propertiesFile.exists()) {
				// read content
				try (FileReader reader = new FileReader(propertiesFile)) {
					properties_l.load(reader);
				} catch (final IOException ioe) {
					ContentManager.LOGGER.warn("Can not load properties file " + propertiesFile.getAbsolutePath() + ".", ioe);
				}
			} else {
				// not a file, try as a class
				contentManagerClass = contentProperty;
			}
		}

		final String innerClass = properties_l.getProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY);
		if (innerClass != null) {
			ContentManager.LOGGER.debug("Loading content manager from properties file " + contentProperty + " containing " + properties_l);
			contentManagerClass = innerClass;
		}

		ContentManager.LOGGER.info("Using content manager class: " + contentManagerClass + " with properties " + properties_l);

		// try to load the given manager
		final Class<?> theClass = Class.forName(contentManagerClass);
		if (!ContentManagerInterface.class.isAssignableFrom(theClass)) {
			final String message = "Class " + theClass + " does not implement ContentManagerInterface. Unable to instanciate content manager.";
			ContentManager.LOGGER.error(message);
			throw new IllegalArgumentException(message);
		}

		this.manager = (ContentManagerInterface) theClass.newInstance();
		// forward properties to the manager
		this.manager.initialize(properties_l);
	}


	@Override
	public URI create(final InputStream input, final Resource resource, final Map<String, Object> parameters) throws IOException {
		return this.manager.create(input, resource, parameters);
	}


	@Override
	public InputStream read(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		return this.manager.read(uri, resource, parameters);
	}


	@Override
	public URI update(final InputStream input, final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		return this.manager.update(input, uri, resource, parameters);
	}


	@Override
	public boolean delete(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		return this.manager.delete(uri, resource, parameters);
	}


	@Override
	public File readLocalExistingFile(final URI uri, final Resource resource, final Map<String, Object> parameters) {
		return this.manager.readLocalExistingFile(uri, resource, parameters);
	}


	// Methods that write content
	/**
	 * Write the input stream to the content manager and annotate the WebLab resource to link it to the native content
	 *
	 * @param res
	 *            standard WebLab Resource
	 * @param content
	 *            a file which could be consumed to get the raw content
	 * @return the URI of the content (for information)
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#create()) instead
	 */
	@Deprecated
	public URI writeNativeContent(final File content, final Resource res) throws WebLabCheckedException {
		try (final FileInputStream fis = FileUtils.openInputStream(content)) {
			return this.writeNativeContent(fis, res);
		} catch (final IOException ioe) {
			ContentManager.LOGGER.error("Unable to save native content file " + content.getPath() + ".", ioe);
			throw new WebLabCheckedException("Unable to save native content file " + content.getPath() + ".", ioe);
		}
	}


	/**
	 * Write the input stream to the content manager and annotate the WebLab resource to link it to the native content
	 *
	 * @param res
	 *            standard WebLab Resource
	 * @param content
	 *            an InputStream which could be consumed to get the raw content
	 * @return the URI of the content (for information)
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#create()) instead
	 */
	@Deprecated
	public URI writeNativeContent(final InputStream content, final Resource res) throws WebLabCheckedException {
		final URI destURI = this.create0(content, res, null);
		final WProcessingAnnotator wpa = new WProcessingAnnotator(res);
		wpa.writeNativeContent(destURI);
		return destURI;
	}


	/**
	 * Write the input stream to the content manager and annotate the WebLab resource to link it to the normalised content
	 *
	 * @param res
	 *            standard WebLab Resource
	 * @param content
	 *            a File which could be consumed to get the raw content
	 * @return the URI of the content (for information)
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#create()) instead
	 */
	@Deprecated
	public URI writeNormalisedContent(final File content, final Resource res) throws WebLabCheckedException {
		try (final FileInputStream fis = FileUtils.openInputStream(content)) {
			return this.writeNormalisedContent(fis, res);
		} catch (final IOException ioe) {
			ContentManager.LOGGER.error("Unable to save normalised content file " + content.getPath() + ".", ioe);
			throw new WebLabCheckedException("Unable to save normalised content file " + content.getPath() + ".", ioe);
		}
	}


	/**
	 * Write the input stream to the content manager and annotate the WebLab resource to link it to the normalised content
	 *
	 * @param res
	 *            standard WebLab Resource
	 * @param content
	 *            an InputStream which could be consumed to get the raw content
	 * @return the URI of the content (for information)
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#create()) instead
	 */
	@Deprecated
	public URI writeNormalisedContent(final InputStream content, final Resource res) throws WebLabCheckedException {
		final URI destURI = this.create0(content, res, null);
		final WProcessingAnnotator wpa = new WProcessingAnnotator(res);
		wpa.writeNormalisedContent(destURI);
		return destURI;
	}


	/**
	 * Read annotation on the input resource and get access to its native content through a JAVA file. Only read access are guaranteed on the returned File
	 *
	 * @param res
	 *            a standard WebLab Resource
	 * @return a java File object with read rights
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#read()) instead
	 */
	@Deprecated
	public File readNativeContent(final Resource res) throws WebLabCheckedException {
		final WProcessingAnnotator wpa = new WProcessingAnnotator(res);
		final Value<URI> values = wpa.readNativeContent();
		if ((values == null) || (!values.hasValue())) {
			throw new WebLabCheckedException("There is no native content defined on this resource [" + res.getUri() + "]");
		}
		if (values.size() > 1) {
			throw new WebLabCheckedException("There are multiple native content defined on this resource [" + res.getUri() + "]:" + values);
		}

		final URI uri = values.getValues().get(0);
		return this.readContent(uri, res);
	}


	/**
	 * Read annotation on the input resource and get access to its normalised content through a JAVA file. Only read access are guaranteed on the returned File
	 *
	 * @param res
	 *            a standard WebLab Resource
	 * @return a java File object with read rights
	 * @throws WebLabCheckedException
	 * @deprecated This method modifies the WebLab resource, use @link(ContentManager#read()) instead
	 */
	@Deprecated
	public File readNormalisedContent(final Resource res) throws WebLabCheckedException {
		final WProcessingAnnotator wpa = new WProcessingAnnotator(res);
		final Value<URI> values = wpa.readNormalisedContent();
		if ((values == null) || (!values.hasValue())) {
			throw new WebLabCheckedException("There is no normalised content defined on this resource [" + res.getUri() + "]");
		}
		if (values.size() > 1) {
			ContentManager.LOGGER.warn("There are multiple normalised content defined on this resource [" + res.getUri() + "]:" + values);
		}

		final URI uri = values.firstTypedValue();
		return this.readContent(uri, res);
	}


	/**
	 * @param contentURI
	 *            The URI of a native or normalised content to read
	 * @param res
	 *            The resource used to guess mime type for instance
	 * @return The file
	 * @throws WebLabCheckedException
	 * @deprecated use @link(ContentManager#read()) instead
	 */
	@Deprecated
	public File readContent(final URI contentURI, final Resource res) throws WebLabCheckedException {
		final File file;
		if (this.manager instanceof ContentReader) {
			// check if old api supported ?
			file = ((ContentReader) this.manager).readContent(contentURI);
		} else {
			file = ContentManager.createTMPFile(this.read0(contentURI, res, null));
		}
		return file;
	}


	/**
	 * Accessor to the internally used content reader.
	 * Reserved to advanced user, those who need to read other content than WebLab normalised and native content.
	 *
	 * @return the internally used content writer.
	 * @deprecated use @link(ContentManager#read()) instead
	 */
	@Deprecated
	public ContentReader getReader() {
		return new ContentReader() {


			@Override
			public File readContent(final URI destUri) throws WebLabCheckedException {
				return ContentManager.this.readContent(destUri, null);
			}
		};
	}


	/**
	 * Accessor to the internally used content writer.
	 * Reserved to advanced user, those who need to write other content than WebLab normalised and native content.
	 *
	 * @return the internally used content writer.
	 * @deprecated use @link(ContentManager#create()) instead
	 */
	@Deprecated
	public ContentWriter getWriter() {
		return new ContentWriter() {


			@Override
			public URI writeExposedContent(final InputStream content, final Resource resource) throws WebLabCheckedException {
				return ContentManager.this.create0(content, resource, null);
			}


			@Override
			public URI writeContent(final InputStream content, final Resource resource) throws WebLabCheckedException {
				return ContentManager.this.create0(content, resource, null);
			}
		};
	}


	/**
	 * Create a temporary file containing the input stream, closes the input stream then.
	 *
	 * @param input
	 *            The input stream to store as temp file
	 * @return a temporary file
	 * @throws WebLabCheckedException
	 *             If an error occured reading the stream or creating the file.
	 */
	@Deprecated
	private static File createTMPFile(final InputStream input) throws WebLabCheckedException {
		final File file;
		try {
			file = File.createTempFile("todelete-" + UUID.randomUUID().toString(), ".tmp");
			FileUtils.copyInputStreamToFile(input, file);
			file.deleteOnExit();
		} catch (final IOException ioe) {
			throw new WebLabCheckedException(ioe);
		}
		return file;
	}


	/**
	 * To keep backward compatibility in API
	 *
	 * @param content
	 * @param res
	 * @param object
	 * @return
	 * @throws WebLabCheckedException
	 */
	@Deprecated
	URI create0(final InputStream content, final Resource res, final Map<String, Object> parameters) throws WebLabCheckedException {
		try {
			return this.create(content, res, parameters);
		} catch (final IOException ioe) {
			throw new WebLabCheckedException(ioe);
		}
	}


	/**
	 * To keep backward compatibility in API
	 *
	 * @param content
	 * @param res
	 * @param object
	 * @return
	 * @throws WebLabCheckedException
	 */
	@Deprecated
	private InputStream read0(final URI uri, final Resource res, final Map<String, Object> parameters) throws WebLabCheckedException {
		try {
			final InputStream stream = this.read(uri, res, parameters);
			if (stream == null) {
				throw new IOException("URI " + uri + " not found.");
			}
			return stream;
		} catch (final IOException ioe) {
			throw new WebLabCheckedException(ioe);
		}
	}

}
