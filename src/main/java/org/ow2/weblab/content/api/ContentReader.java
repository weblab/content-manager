/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api;

import java.io.File;
import java.net.URI;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;

/**
 * An interface to be implemented for use with ContentManager.
 *
 * It enable to read content from a common content repository.
 *
 * @deprecated use org.ow2.weblab.content.api.ContentManagerInterface instead
 */
@Deprecated
public interface ContentReader {


	/**
	 * Abstract method to implement: the class is expected to save the content and return the URI of the save content.
	 *
	 * @param destUri
	 *            the URI of the content to read
	 * @return a java File which should be ready to be used with read rights.
	 * @throws WebLabCheckedException
	 */
	public File readContent(final URI destUri) throws WebLabCheckedException;
}
