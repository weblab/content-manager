/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api;

import java.io.InputStream;
import java.net.URI;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Resource;


/**
 * An interface to be implemented for use with ContentManager.
 *
 * It enable to save content in a common content repository.
 * @deprecated use org.ow2.weblab.content.api.ContentManagerInterface instead
 */
@Deprecated
public interface ContentWriter {


	/**
	 * Abstract method to implement : the implementation is expected to save the content in common content repository of the platform and return the URI of the
	 * save content.
	 *
	 * @param content
	 *            an inputStream leading to the raw bytes of the content
	 * @param resource
	 *            a valid WebLab resource associated with the content
	 * @return the URI of the content saved (which will be used for reading the content later on)
	 * @throws WebLabCheckedException
	 */
	public URI writeContent(final InputStream content, final Resource resource) throws WebLabCheckedException;


	/**
	 * Abstract method to implement : the implementation is expected to save the content in a repository which enables end-user to access the content and return
	 * the final URI of the save content (used for client).
	 *
	 * @param content
	 *            an inputStream leading to the raw bytes of the content
	 * @param resource
	 *            a valid WebLab resource associated with the content
	 * @return the URI of the content saved (which will be used for accessing the content from end-user side)
	 * @throws WebLabCheckedException
	 */
	public URI writeExposedContent(final InputStream content, final Resource resource) throws WebLabCheckedException;

}
