/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.content.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.Properties;

import org.ow2.weblab.core.model.Resource;

/**
 * Provide interface to manage content using CRUD methods.
 * Any class implementing this interface MUST NOT update NOR change the WebLab resource.
 *
 * @author asaval, ymombrun
 */
public interface ContentManagerInterface {


	/**
	 * Store the content of the input and create a new URI corresponding to this stored data.
	 * Any implementation of this method MUST NOT update NOR change the WebLab resource
	 *
	 * @param input
	 *            data to store
	 * @return an URI of the stored data or null if the creation failed.
	 * @throws IOException
	 *             If an error occurred creating the file at <code>uri</code>.
	 */
	public URI create(final InputStream input) throws IOException;


	/**
	 * Read the data stored with the following URI.
	 *
	 * @param uri
	 *            of the stored data
	 * @return An output stream of the data or null if the data does not exist.
	 * @throws IOException
	 *             If an error occurred reading the file at <code>uri</code>.
	 */
	public InputStream read(final URI uri) throws IOException;


	/**
	 * Update data stored at the following URI.
	 *
	 * @param input
	 *            data to store
	 * @param uri
	 *            of the stored data
	 * @return the same URI or null if the update failed.
	 * @throws IOException
	 *             If an error occurred updating the file at <code>uri</code>.
	 */
	public URI update(final InputStream input, final URI uri) throws IOException;


	/**
	 * Delete data stored with the following URI.
	 *
	 * @param uri
	 *            of the stored data
	 * @return true if data has been successfully deleted else false.
	 * @throws IOException
	 *             If an error occurred reading the file at <code>uri</code>.
	 */
	public boolean delete(final URI uri) throws IOException;


	/**
	 * Store the content of the input and create a new URI corresponding to this stored data.
	 * Any implementation of this method MUST NOT update NOR change the WebLab resource
	 *
	 * @param input
	 *            data to store
	 * @param resource
	 *            WebLab resource linked to this resource, it can be null
	 * @param parameters
	 *            map of optional parameters, it can be null
	 * @return an URI of the stored data or null if the creation failed.
	 * @throws IOException
	 *             If an error occurred creating the file at <code>uri</code>.
	 */
	public URI create(final InputStream input, final Resource resource, final Map<String, Object> parameters) throws IOException;


	/**
	 * Read the data stored with the following URI.
	 *
	 * @param uri
	 *            of the stored data
	 * @param resource
	 *            WebLab resource linked to this resource, it can be null
	 * @param parameters
	 *            map of optional parameters, it can be null
	 * @return An output stream of the data or null if the data does not exist.
	 * @throws IOException
	 *             If an error occurred reading the file at <code>uri</code>.
	 */
	public InputStream read(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException;


	/**
	 * Read the data stored with the following URI and return a file only if the URI can be resolved locally.
	 * The file returned is the one stored locally when the URI can be resolved to a local file path.
	 * Any remote implementation (like FTP, WebDAV...) SHOULD return null here.
	 * It is the charge of the user to download the file and make a copy if necessary for these implementation.
	 * This can only be prevented in case of local file path.
	 *
	 * @param uri
	 *            of the access the data
	 * @param resource
	 *            WebLab resource linked to this resource, it can be null.
	 * @param parameters
	 *            map of optional parameters, it can be null
	 * @return A File or null if the URI cannot be resolved to a local file.
	 */
	public File readLocalExistingFile(final URI uri, final Resource resource, final Map<String, Object> parameters);


	/**
	 * Update data stored at the following URI.
	 *
	 * @param input
	 *            data to store
	 * @param uri
	 *            of the stored data
	 * @param resource
	 *            WebLab resource linked to this resource, it can be null
	 * @param parameters
	 *            map of optional parameters, it can be null
	 * @return the same URI or null if the update failed.
	 * @throws IOException
	 *             If an error occurred updating the file at <code>uri</code>.
	 */
	public URI update(final InputStream input, final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException;


	/**
	 * Delete data stored with the following URI.
	 *
	 * @param uri
	 *            of the stored data
	 * @param resource
	 *            WebLab resource linked to this resource, it can be null
	 * @param parameters
	 *            map of optional parameters, it can be null
	 * @return true if data has been successfully deleted else false.
	 * @throws IOException
	 *             If an error occurred removing the file at <code>uri</code>.
	 */
	public boolean delete(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException;


	/**
	 * Forward initialisation properties to the manager, this method is called after the empty constructor.
	 * Any implementation might override the provided properties with specific values coming from the environment.
	 *
	 * @param properties
	 *            A map of property to be used as baseline for configuration.
	 */
	public void initialize(final Properties properties);

}