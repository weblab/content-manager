/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api.thread;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;

public class GetFromContentManagerThread implements Callable<Long> {


	private final File inFile = new File("src/test/resources/Virtuoso_paper.pdf");


	private final int maxIteration;


	public GetFromContentManagerThread(final int maxIteration) {
		super();
		this.maxIteration = maxIteration;
	}


	@Override
	public Long call() throws Exception {
		long cumulatedTime = 0;
		for (int k = 0; k < this.maxIteration; k++) {
			final Resource doc = WebLabResourceFactory.createResource("TestFileContentManager", "thread-" + this, Document.class);
			final long start = System.currentTimeMillis();

			final ContentManager mngr = ContentManager.getInstance();
			try (final FileInputStream stream = new FileInputStream(this.inFile)) {
				final URI uri = mngr.create(stream, doc, null);
				new WProcessingAnnotator(doc).writeNativeContent(uri);
			}

			final File f = mngr.readLocalExistingFile(new WProcessingAnnotator(doc).readNativeContent().firstTypedValue(), null, null);
			Assert.assertTrue(FileUtils.contentEquals(this.inFile, f));
			cumulatedTime += (System.currentTimeMillis() - start);
		}
		return Long.valueOf(cumulatedTime / this.maxIteration);
	}

}
