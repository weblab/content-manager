/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api.thread;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

import org.ow2.weblab.content.api.ContentManager;

public class SaveOnContentManagerThread implements Callable<Long> {


	private final File in = new File("src/test/resources/Virtuoso_paper.pdf");


	private final int maxIteration;


	public SaveOnContentManagerThread(final int maxIteration) {
		super();
		this.maxIteration = maxIteration;
	}


	@Override
	public Long call() throws Exception {
		long cumulatedTime = 0;
		for (int k = 0; k < this.maxIteration; k++) {
			final long start = System.currentTimeMillis();
			try (final FileInputStream stream = new FileInputStream(this.in)) {
				ContentManager.getInstance().create(stream);
			}
			cumulatedTime += (System.currentTimeMillis() - start);
		}
		return Long.valueOf(cumulatedTime / this.maxIteration);
	}

}
