/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;

import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.model.Resource;

public class MockContentManager extends ContentManagerAdapter {


	public URI writeContent(final InputStream content, final Resource resource) {
		throw new WebLabUncheckedException("dummy");
	}


	public URI writeExposedContent(final InputStream content, final Resource resource) {
		throw new WebLabUncheckedException("dummy");
	}


	public File readContent(final URI destUri) {
		throw new WebLabUncheckedException("dummy");
	}


	@Override
	public URI create(final InputStream input, final Resource resource, final Map<String, Object> parameters) throws IOException {
		throw new WebLabUncheckedException("dummy");
	}


	@Override
	public InputStream read(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		throw new WebLabUncheckedException("dummy");
	}


	@Override
	public URI update(final InputStream input, final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		throw new WebLabUncheckedException("dummy");
	}


	@Override
	public boolean delete(final URI uri, final Resource resource, final Map<String, Object> parameters) throws IOException {
		throw new WebLabUncheckedException("dummy");
	}


	@Override
	public File readLocalExistingFile(final URI uri, final Resource resource, final Map<String, Object> parameters) {
		throw new WebLabUncheckedException("dummy");
	}

}
