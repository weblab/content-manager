/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.content.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;

@Deprecated
public class DeprecatedContentManagerTest {


	protected static final String CONTENT_MANAGER_CONFIGURED_FOLDER = "target/data/content";


	protected static Log logger = LogFactory.getLog(DeprecatedContentManagerTest.class);


	protected ContentManager mngr = ContentManager.getInstance();


	protected File testFile = new File("src/test/resources/Virtuoso_paper.pdf");



	@AfterClass
	public static void deleteData() {
		final boolean deleted = FileUtils.deleteQuietly(new File(DeprecatedContentManagerTest.CONTENT_MANAGER_CONFIGURED_FOLDER));
		if (deleted) {
			DeprecatedContentManagerTest.logger.debug(DeprecatedContentManagerTest.CONTENT_MANAGER_CONFIGURED_FOLDER + " has been deleted correctly.");
		}
	}


	@Test
	public void testSaveNativeContentOnOneFile() throws IOException, WebLabCheckedException {
		final Resource res1 = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
		final URI dest = this.mngr.writeNativeContent(this.testFile, res1);
		Assert.assertNotNull(dest);
		new WebLabMarshaller().marshalResource(res1, new File("target/testSaveNativeContentOnOneFileDeprecated.xml"));
		final File f1 = this.mngr.readContent(dest, res1);
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f1));
		final File f2 = this.mngr.readLocalExistingFile(dest, null, null);
		Assert.assertEquals(FileUtils.readFileToString(f1), FileUtils.readFileToString(f2));
		final File f3 = this.mngr.readNativeContent(res1);
		Assert.assertEquals(FileUtils.readFileToString(f2), FileUtils.readFileToString(f3));

		this.mngr.update(IOUtils.toInputStream("not the pdf!"), dest);
		final File f4 = this.mngr.readNativeContent(res1);
		Assert.assertNotEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f4));
	}


	@Test
	public void testSaveNormalisedContentOnOneFile() throws IOException, WebLabCheckedException {
		final Resource res1 = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
		final URI dest = this.mngr.writeNormalisedContent(this.testFile, res1);
		Assert.assertNotNull(dest);
		new WebLabMarshaller().marshalResource(res1, new File("target/testSaveNativeContentOnOneFileDeprecated.xml"));
		final File f1 = this.mngr.readContent(dest, res1);
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f1));
		final File f2 = this.mngr.readLocalExistingFile(dest, null, null);
		Assert.assertEquals(FileUtils.readFileToString(f1), FileUtils.readFileToString(f2));
		final File f3 = this.mngr.readNormalisedContent(res1);
		Assert.assertEquals(FileUtils.readFileToString(f2), FileUtils.readFileToString(f3));

		this.mngr.update(IOUtils.toInputStream("not the pdf!"), dest);
		final File f4 = this.mngr.readNormalisedContent(res1);
		Assert.assertNotEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f4));
	}


	@Test
	public void testSaveSomeFiles() throws WebLabCheckedException {
		for (int i = 0; i < 20; i++) {
			final Resource doc = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
			final URI dest = this.mngr.writeNativeContent(this.testFile, doc);
			Assert.assertNotNull(dest);
			final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
			final Value<URI> nativeContentUri = wpa.readNativeContent();
			Assert.assertNotNull(nativeContentUri);
			Assert.assertEquals(1, nativeContentUri.size());
		}
	}


	@Test(expected = WebLabCheckedException.class)
	public void testNoSchemeUri() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("test", "Doc", Document.class);
		new WProcessingAnnotator(doc).writeNativeContent(URI.create("Bad"));
		this.mngr.readNativeContent(doc);
	}


	@Test
	public void testNetworkSchemeUri() throws WebLabCheckedException {
		final String url = "http://weblab-project.org";
		Assume.assumeTrue(DeprecatedContentManagerTest.ping(url, 6000));
		final Document doc = WebLabResourceFactory.createResource("test2", "Doc", Document.class);
		new WProcessingAnnotator(doc).writeNativeContent(URI.create(url));
		this.mngr.readNativeContent(doc);
	}


	@Test(expected = WebLabCheckedException.class)
	public void testNonExistingFileSchemeUri() throws WebLabCheckedException {
		final Document doc = WebLabResourceFactory.createResource("test3", "Doc", Document.class);
		new WProcessingAnnotator(doc).writeNativeContent(URI.create("file:///this.file.does.not.exists/I-hope-So/OrThisTestWillPath"));
		final File file = this.mngr.readLocalExistingFile(new WProcessingAnnotator(doc).readNativeContent().firstTypedValue(), null, null);
		Assert.assertNull(file);
		this.mngr.readNativeContent(doc);
	}


	@Test
	public void testWithOldInterfaces() throws Exception {
		final Resource doc = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
		final URI dest;
		try (final InputStream stream = FileUtils.openInputStream(this.testFile)) {
			dest = this.mngr.getWriter().writeContent(stream, doc);
		}
		File file = this.mngr.getReader().readContent(dest);
		Assert.assertTrue(FileUtils.contentEquals(file, this.testFile));
	}



	/**
	 * Pings a HTTP URL. This effectively sends a HEAD request and returns <code>true</code> if the response code is in the 200-399 range.
	 *
	 * @param url
	 *            The HTTP URL to be pinged.
	 * @param timeout
	 *            The timeout in millis for both the connection timeout and the response read timeout. Note that the total timeout is effectively two times the given timeout.
	 * @return <code>true</code> if the given HTTP URL has returned response code 200-399 on a HEAD request within the given timeout, otherwise <code>false</code>.
	 * @see "http://stackoverflow.com/questions/3584210/preferred-java-way-to-ping-a-http-url-for-availability"
	 */
	public static boolean ping(final String url, final int timeout) {
		try {
			final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setRequestMethod("HEAD");
			final int responseCode = connection.getResponseCode();
			return ((200 <= responseCode) && (responseCode <= 399));
		} catch (final IOException exception) {
			return false;
		}
	}

}
