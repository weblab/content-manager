/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.content.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.ow2.weblab.content.api.thread.GetFromContentManagerThread;
import org.ow2.weblab.content.api.thread.SaveOnContentManagerThread;
import org.ow2.weblab.content.impl.FileContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

public class ContentManagerTest {


	protected static final String CONTENT_MANAGER_CONFIGURED_FOLDER = "target/data/content";


	protected static Log logger = LogFactory.getLog(ContentManagerTest.class);


	protected ContentManager mngr = ContentManager.getInstance();


	protected File testFile = new File("src/test/resources/Virtuoso_paper.pdf");



	@AfterClass
	public static void deleteData() {
		final boolean deleted = FileUtils.deleteQuietly(new File(ContentManagerTest.CONTENT_MANAGER_CONFIGURED_FOLDER));
		if (deleted) {
			ContentManagerTest.logger.debug(ContentManagerTest.CONTENT_MANAGER_CONFIGURED_FOLDER + " has been deleted correctly.");
		}
	}


	@Test
	public void testSaveNativeContentOnOneFile() throws IOException, WebLabCheckedException {
		final Resource res1 = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
		final URI dest;
		try (final InputStream in = FileUtils.openInputStream(this.testFile)) {
			dest = this.mngr.create(in);
		}
		Assert.assertNotNull(dest);
		new WProcessingAnnotator(res1).writeNativeContent(dest);
		new WebLabMarshaller().marshalResource(res1, new File("target/testSaveNativeContentOnOneFile.xml"));
		final File f1 = this.mngr.readLocalExistingFile(dest, res1, new HashMap<String, Object>());
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f1));
		final File f2 = this.mngr.readLocalExistingFile(dest, null, null);
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f2));
		final File f3 = this.mngr.readLocalExistingFile(new WProcessingAnnotator(res1).readNativeContent().firstTypedValue(), null, null);
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f3));
		final File f4 = this.mngr.readLocalExistingFile(f3.getAbsoluteFile().toURI(), null, null); // Try with a full URI
		Assert.assertEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f4));

		this.mngr.update(IOUtils.toInputStream("not the pdf!"), dest);
		final File f5 = this.mngr.readLocalExistingFile(new WProcessingAnnotator(res1).readNativeContent().firstTypedValue(), null, null);
		Assert.assertNotEquals(FileUtils.readFileToString(this.testFile), FileUtils.readFileToString(f5));

		this.mngr.delete(dest);
		final File f6 = this.mngr.readLocalExistingFile(dest, null, null);
		Assert.assertNull(f6);

		this.mngr.delete(dest);

		try {
			this.mngr.read(dest);
			Assert.fail("The file has been removed. It should not exist!");
		} catch (final IOException ioe) {
			// The file has been removed. It should not exist!
		}

		Assert.assertTrue("Error creating a directory where a file is expected...", f1.mkdirs());
		Assert.assertTrue("Error creating a directory where a file is expected...", f1.exists() && f1.isDirectory());
		try {
			this.mngr.read(dest);
			Assert.fail("A directory is here it should not work");
		} catch (final IOException ioe) {
			// A directory is here it should not work!
		}

		try {
			this.mngr.update(IOUtils.toInputStream("Should fail"), dest);
			Assert.fail("A directory is here it should not work");
		} catch (final IOException ioe) {
			// A directory is here it should not work!
		}
	}



	@Test
	public void testSaveSomeFiles() throws IOException {
		for (int i = 0; i < 20; i++) {
			final Resource doc = WebLabResourceFactory.createResource("TestFileContentManager", "testSaveOneFile", Document.class);
			final URI dest;
			try (final FileInputStream stream = new FileInputStream(this.testFile)) {
				dest = this.mngr.create(stream);
				new WProcessingAnnotator(doc).writeNativeContent(dest);
			}
			Assert.assertNotNull(dest);
			final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
			final Value<URI> nativeContentUri = wpa.readNativeContent();
			Assert.assertNotNull(nativeContentUri);
			Assert.assertEquals(1, nativeContentUri.size());
		}
	}


	@Test
	public void addSomeDocumentsInThread() throws InterruptedException, ExecutionException {
		final int nbThread = 5;
		final int maxIteration = 10;
		final List<Callable<Long>> save = new ArrayList<>();

		for (int i = 0; i < nbThread; i++) {
			save.add(new SaveOnContentManagerThread(maxIteration));
		}

		final ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		final List<Future<Long>> results = executor.invokeAll(save, 5, TimeUnit.MINUTES);
		long metaDuration = 0;
		for (final Future<Long> future : results) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			metaDuration += future.get().longValue();
		}
		ContentManagerTest.logger.info("Threads executed correctly.");
		ContentManagerTest.logger.info("Mean save time over " + (nbThread * maxIteration) + " save: " + (metaDuration / nbThread) + " ms");
	}


	@Test
	public void testGetADoc() throws IOException {
		final Document doc = WebLabResourceFactory.createResource("TestFileContentManager", "testGetADoc", Document.class);
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			new WProcessingAnnotator(doc).writeNormalisedContent(this.mngr.create(stream));
		}
		try (final InputStream stream = this.mngr.read(new WProcessingAnnotator(doc).readNormalisedContent().firstTypedValue()); final InputStream stream2 = FileUtils.openInputStream(this.testFile)) {
			Assert.assertTrue(IOUtils.contentEquals(stream, stream2));
		}

	}


	@Test
	public void testSomeDocs() throws IOException {
		for (int i = 0; i < 20; i++) {
			final Document doc = WebLabResourceFactory.createResource("TestFileContentManager", "testSomeDocs" + i, Document.class);
			final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
			try (final InputStream stream = FileUtils.openInputStream(this.testFile)) {
				wpa.writeNativeContent(this.mngr.create(stream));
			}
			final File f = this.mngr.readLocalExistingFile(wpa.readNativeContent().firstTypedValue(), null, null);

			Assert.assertTrue(FileUtils.contentEquals(this.testFile, f));
		}
	}


	@Test
	public void getSomeDocumentsInThread() throws InterruptedException, ExecutionException {
		final int nbThread = 5;
		final int maxIteration = 10;
		final List<Callable<Long>> get = new ArrayList<>();

		for (int i = 0; i < nbThread; i++) {
			get.add(new GetFromContentManagerThread(maxIteration));
		}

		final ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		final List<Future<Long>> results = executor.invokeAll(get, 5, TimeUnit.MINUTES);
		long metaDuration = 0;
		for (final Future<Long> future : results) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			metaDuration += future.get().longValue();
		}
		ContentManagerTest.logger.info("Threads executed correctly.");
		ContentManagerTest.logger.info("Mean get time over " + (nbThread * maxIteration) + " save: " + (metaDuration / nbThread) + " ms");
	}


	@Test(expected = IOException.class)
	public void testReadNoSchemeUri() throws IOException {
		final File file = this.mngr.readLocalExistingFile(URI.create("Bad"), null, null);
		Assert.assertNull(file);
		this.mngr.read(URI.create("Bad"));
	}


	@Test(expected = IOException.class)
	public void testUpdateNoSchemeUri() throws IOException {
		this.mngr.update(IOUtils.toInputStream("Bad"), URI.create("Bad"));
	}


	@Test(expected = IOException.class)
	public void testBadShemeUri() throws IOException {
		final File file = this.mngr.readLocalExistingFile(URI.create("bad:veryBad"), null, null);
		Assert.assertNull(file);
		this.mngr.read(URI.create("bad:veryBad"));
	}


	@Test(expected = IOException.class)
	public void testUpdateBadSchemeUri() throws IOException {
		this.mngr.update(IOUtils.toInputStream("bad:veryBad"), URI.create("bad:veryBad"));
	}


	@Test(expected = IOException.class)
	public void testUpdateNetworkSchemeUri() throws IOException {
		final String url = "http://weblab-project.org";
		Assume.assumeTrue(ContentManagerTest.ping(url, 6000));
		this.mngr.update(IOUtils.toInputStream(url), URI.create(url));
	}



	@Test
	public void testNetworkSchemeUri() throws IOException {
		final String url = "http://weblab-project.org";
		Assume.assumeTrue(ContentManagerTest.ping(url, 6000));
		final File file = this.mngr.readLocalExistingFile(URI.create(url), null, null);
		Assert.assertNull(file);
		this.mngr.read(URI.create(url));
	}

	@Test
	public void testReadNullLocal() {
		final File file = this.mngr.readLocalExistingFile(null, null, null);
		Assert.assertNull(file);
	}


	@Test(expected = IOException.class)
	public void testNonExistingFileShemeUri() throws IOException {
		final Document doc = WebLabResourceFactory.createResource("test3", "Doc", Document.class);
		new WProcessingAnnotator(doc).writeNativeContent(URI.create("file:///this.file.does.not.exists/I-hope-So/OrThisTestWillPath"));
		final File file = this.mngr.readLocalExistingFile(new WProcessingAnnotator(doc).readNativeContent().firstTypedValue(), null, null);
		Assert.assertNull(file);
		this.mngr.read(new WProcessingAnnotator(doc).readNativeContent().firstTypedValue());
	}


	@Test
	public void testWriteLocal() throws Exception {
		// 1. Copy a file inside the content manager manually
		final File destFile = new File(ContentManagerTest.CONTENT_MANAGER_CONFIGURED_FOLDER, "testWriteLocal.pdf");
		FileUtils.copyFile(this.testFile, destFile);

		// Test with a Boolean parameter (and null input stream, should not fail)
		final Document doc1 = WebLabResourceFactory.createResource("testWriteLocal", "booleanTrue", Document.class);
		new DublinCoreAnnotator(doc1).writeSource(destFile.getAbsolutePath());
		final URI uri1 = this.mngr.create(null, doc1, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) Boolean.TRUE));
		Assert.assertEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri1, null, null).getAbsoluteFile());

		// Test with a String parameter (and an non used input stream)
		final Document doc2 = WebLabResourceFactory.createResource("testWriteLocal", "stringTrue", Document.class);
		new DublinCoreAnnotator(doc2).writeSource(destFile.getAbsolutePath());
		final URI uri2;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri2 = this.mngr.create(stream, doc2, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) "true"));
		}
		Assert.assertEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri2, null, null).getAbsoluteFile());

		// Test with false
		final Document doc3 = WebLabResourceFactory.createResource("testWriteLocal", "stringFalse", Document.class);
		new DublinCoreAnnotator(doc3).writeSource(destFile.getAbsolutePath());
		final URI uri3;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri3 = this.mngr.create(stream, doc3, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) "false"));
		}
		Assert.assertNotEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri3, null, null).getAbsoluteFile());
		Assert.assertTrue(FileUtils.contentEquals(destFile, this.mngr.readLocalExistingFile(uri3, null, null)));

		// Test without resource (use the stream)
		final URI uri;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri = this.mngr.create(stream, null, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) "true"));
		}
		Assert.assertNotEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri, null, null).getAbsoluteFile());
		Assert.assertTrue(FileUtils.contentEquals(destFile, this.mngr.readLocalExistingFile(uri, null, null)));


		// Test without source (use the stream)
		final Document doc4 = WebLabResourceFactory.createResource("testWriteLocal", "withoutSource", Document.class);
		final URI uri4;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri4 = this.mngr.create(stream, doc4, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) "true"));
		}
		Assert.assertNotEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri4, null, null).getAbsoluteFile());
		Assert.assertTrue(FileUtils.contentEquals(destFile, this.mngr.readLocalExistingFile(uri4, null, null)));


		// Test with invalid source (use the stream)
		final Document doc5 = WebLabResourceFactory.createResource("testWriteLocal", "invalidSource", Document.class);
		new DublinCoreAnnotator(doc5).writeSource("there is no file here, that's sure!");
		final URI uri5;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri5 = this.mngr.create(stream, doc5, Collections.singletonMap(FileContentManager.LOCAL_CONTENT, (Object) "true"));
		}
		Assert.assertNotEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri5, null, null).getAbsoluteFile());
		Assert.assertTrue(FileUtils.contentEquals(destFile, this.mngr.readLocalExistingFile(uri5, null, null)));

		// Test with empty parameters source (use the stream)
		final Document doc6 = WebLabResourceFactory.createResource("testWriteLocal", "emptyParam", Document.class);
		new DublinCoreAnnotator(doc6).writeSource(destFile.getAbsolutePath());
		final URI uri6;
		try (final FileInputStream stream = new FileInputStream(this.testFile)) {
			uri6 = this.mngr.create(stream, doc6, new HashMap<String, Object>());
		}
		Assert.assertNotEquals(destFile.getAbsoluteFile(), this.mngr.readLocalExistingFile(uri6, null, null).getAbsoluteFile());
		Assert.assertTrue(FileUtils.contentEquals(destFile, this.mngr.readLocalExistingFile(uri6, null, null)));
	}



	/**
	 * Pings a HTTP URL. This effectively sends a HEAD request and returns <code>true</code> if the response code is in the 200-399 range.
	 *
	 * @param url
	 *            The HTTP URL to be pinged.
	 * @param timeout
	 *            The timeout in millis for both the connection timeout and the response read timeout. Note that the total timeout is effectively two times the given timeout.
	 * @return <code>true</code> if the given HTTP URL has returned response code 200-399 on a HEAD request within the given timeout, otherwise <code>false</code>.
	 * @see "http://stackoverflow.com/questions/3584210/preferred-java-way-to-ping-a-http-url-for-availability"
	 */
	public static boolean ping(final String url, final int timeout) {
		try {
			final HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.setRequestMethod("HEAD");
			final int responseCode = connection.getResponseCode();
			return ((200 <= responseCode) && (responseCode <= 399));
		} catch (final IOException exception) {
			return false;
		}
	}

}
