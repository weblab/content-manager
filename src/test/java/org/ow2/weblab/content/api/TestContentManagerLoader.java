/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.content.api;

import java.io.File;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.content.impl.FileContentManager;
import org.ow2.weblab.content.impl.SecureFileContentManager;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;

public class TestContentManagerLoader {


	@Before
	public void prepareProperties() {
		// reset content manager
		ContentManager.instance = null;
		System.clearProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY);
		System.clearProperty(FileContentManager.FOLDER_CONTENT_PATH_PROPERTY_NAME);
	}


	@Test(expected = WebLabUncheckedException.class)
	public void cnfeFromSystemPropery() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "not a class, that's sure!");
		ContentManager.getInstance();
	}

	@Test(expected = IllegalArgumentException.class)
	public void iArgEFromSystemPropery() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "java.lang.Math");
		ContentManager.getInstance();
	}

	@Test(expected = WebLabUncheckedException.class)
	public void ieFromSystemPropery() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "org.ow2.weblab.content.api.NoConstructorMockContentManager");
		ContentManager.getInstance();
	}

	@Test(expected = WebLabUncheckedException.class)
	public void iAccEFromSystemPropery() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "org.ow2.weblab.content.api.PrivateMockContentManager");
		ContentManager.getInstance();
	}

	@Test()
	public void goodClassLoadingFromSystemProperty() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "org.ow2.weblab.content.api.MockContentManager");
		ContentManager.getInstance();
		Assert.assertEquals(MockContentManager.class, ContentManager.getInstance().manager.getClass());
	}


	@Test()
	public void goodClassLoadingFromFile() {
		ContentManager.instance = null;
		ContentManager.getInstance();
		Assert.assertEquals(SecureFileContentManager.class, ContentManager.getInstance().manager.getClass());
	}

	@Test(expected = WebLabUncheckedException.class)
	public void goodClassLoadingFromSystemPropertyButBadFolder() {
		System.setProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY, "org.ow2.weblab.content.impl.FileContentManager");
		System.setProperty(FileContentManager.FOLDER_CONTENT_PATH_PROPERTY_NAME, new File("pom.xml").getAbsolutePath());
		ContentManager.getInstance();
	}


	@After
	public void cleanProperties() {
		// reset content manager
		ContentManager.instance = null;
		System.clearProperty(ContentManager.CONTENT_MANAGER_SYSTEM_PROPERTY);
		System.clearProperty(FileContentManager.FOLDER_CONTENT_PATH_PROPERTY_NAME);
	}
}
